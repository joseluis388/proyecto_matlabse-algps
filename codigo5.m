Ts = 0.01;
t = -2:Ts:2;
f = (t>0);
g = f.*0.6.^t;

%Convolución
cnv = Ts*conv(f,g,'same');

subplot(3,1,1),plot(t,f), ylim([0 1.5]),title('f(t)'),grid on
subplot(3,1,2),plot(t,g,'r'), ylim([0 1.5]),title('g(t)'),grid on
subplot(3,1,3),plot(t,cnv,'g'), ylim([0 1.5]),title('conv(f(t),g(t))'),grid on