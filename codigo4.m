ts =.01;
gox=0; stopx =1/2;
goh=0; stoph =2;
tx=gox : ts : stopx ;
x=exp( - tx ) ;
th=goh : ts : stoph ;
h=exp( - th ) ;
ty=gox+goh : ts : stopx+stoph ;
y=ts *conv(x , h)  ;

subplot (311)
plot ( tx , x)
xlabel ( ' t ' ) , ylabel ( 'x( t ) ' )
subplot (312)
plot ( th , h)
xlabel ( ' t ' ) , ylabel ( 'h( t ) ' )
subplot (313)
plot ( ty , y)
xlabel ( ' t ' ) , ylabel ( 'y( t )=x( t ) *h( t ) ' )