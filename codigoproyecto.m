fs = 5;
duration = 100;
numSamples = duration*fs;

refLoc = [4.6610335 -74.059838 2.630];

truePosition = zeros(numSamples,3);
trueVelocity = zeros(numSamples,3);

gps = gpsSensor('UpdateRate',fs,'ReferenceLocation',refLoc);

position = gps(truePosition,trueVelocity);

t = (0:(numSamples-1))/fs;

subplot(3, 1, 1)
plot(t, position(:,1), ...
     t, ones(numSamples)*refLoc(1))
title('Lectura el sensor GPS')
ylabel('Latitud (grados)')

subplot(3, 1, 2)
plot(t, position(:,2), ...
     t, ones(numSamples)*refLoc(2))
ylabel('Longitud(grados)')

subplot(3, 1, 3)
plot(t, position(:,3), ...
     t, ones(numSamples)*refLoc(3))
ylabel('Altitud (m)')
xlabel('Time (s)')

reset(gps)
gps.DecayFactor = 0.5;
position = gps(truePosition,trueVelocity);

subplot(3, 1, 1)
plot(t, position(:,1), ...
     t, ones(numSamples)*refLoc(1))
title('lectura delsensor - Decay Factor = 0.5')
ylabel('Latitud (grados)')

subplot(3, 1, 2)
plot(t, position(:,2), ...
     t, ones(numSamples)*refLoc(2))
ylabel('Longitud  (grados)')

subplot(3, 1, 3)
plot(t, position(:,3), ...
     t, ones(numSamples)*refLoc(3))
ylabel('Altitude (m)')
xlabel('tiempo(s)')







%analis de la frecuencia de la señal ECG Aplicando la transformada discreta
%de fourirer
GNSS1=position(:,1);
GNSSS1=ones(numSamples)*refLoc(1);
GNSST1=GNSS1+GNSSS1;


GNSS2=position(:,2);
GNSSS2=ones(numSamples)*refLoc(2);
GNSST2=GNSS2+GNSSS2;




GNSS3=position(:,3);
GNSSS3=ones(numSamples)*refLoc(3);
GNSST3=GNSS3+GNSSS3;





muestras = length(GNSST1);
ordenadas=fft(GNSST1,muestras);
abcisas=linspace(0,fs,muestras); %vector lienalmente espaciado 
figure(2);
subplot(3,1,1);
plot(abcisas,ordenadas);
grid;
xlabel("frecuencia");
ylabel("magnitud");
title(" analisis espectral(dominio dela frecuencia)");


muestras1 = length(GNSST2);
ordenadas=fft(GNSST2,muestras1);
abcisas=linspace(0,fs,muestras1); %vector lienalmente espaciado 
figure(2);
subplot(3,1,2);
plot(abcisas,ordenadas);
grid;
xlabel("frecuencia");
ylabel("magnitud");
title(" analisis espectral(dominio dela frecuencia)");




muestras2 = length(GNSST3);
ordenadas=fft(GNSST3,muestras2);
abcisas=linspace(0,fs,muestras2); %vector lienalmente espaciado 
figure(2);
subplot(3,1,3);
plot(abcisas,ordenadas);
grid;
xlabel("frecuencia");
ylabel("magnitud");
title(" analisis espectral(dominio dela frecuencia)");
